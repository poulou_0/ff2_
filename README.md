## Entities
- User
- Book Series
- Book
- Genre (tags)

## Basic relationships
- A book is connected to series with a many-to-many
    - An extra table "book_series" will achieve this many-to-many relationship
- A book series can be a child of a parent series
    - Just like "seasons" in a tv series
- The table "users" will be connected to both "series" AND "books" (many-to-many)
    - A user can follow series without having read any book yet
    - Two extra tables will achieve this many-to-many relationship
        - "user_series" and "user_books"
- A genre will be connected directly to the book and not to the series
    - Thats because a series can start with kids books and go to a different direction over the years
    - We can query the db to fetch series of specific genres
    - The genres will be equal, for the time being, there won't be any parent/child relationship
    - An extra table "book_genre" will achieve this many-to-many relationship

## TODO
- set what will happen to the related entries when delete or update (cascade, restrict etc.)
    - for now, everything is set to "no action"


## Dev Notes
- :O https://mac-blog.org.ua/wordpress-custom-database-table-example-full/
- https://www.wpexplorer.com/wordpress-page-templates-plugin/
- https://wordpress.stackexchange.com/questions/268923/how-do-i-get-user-data-from-a-custom-table-in-the-wordpress-database-by-user-id
